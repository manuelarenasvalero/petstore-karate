@petListByStatus

Feature: pet list on format ID - NAME

Background:
# NOTE - Possible values for 'status' : available, pending, sold.

# SECTION - FUNCTION TO RECREATE OUTPUT AS NUMBER-NAME OF THE PET

	* def fun = 
	"""
	function (input) { 
		var list = [];
		
		for ( i = 0 ; i < input.length ; i++ )
		{
			var element = '{' + input[i].id + ', ' + input[i].name + '}';
			list = karate.appendTo(list, element);
		}
		
		return(list)
	}
	"""
# !SECTION

# SECTION - SEPARATED REQUESTS

# Scenario: list by status 'SOLD'
# 	* param status = 'sold'

# 	Given url SITE
# 	And path ENDPOINT_PET, '/findByStatus'
#   When method GET
#   Then status 200

# 	* print 'sold list = ' , fun(response)

# Scenario: list by status 'PENDING'
# 	* param status = 'pending'
	
# 	Given url SITE
# 	And path ENDPOINT_PET, '/findByStatus'
#   When method GET
#   Then status 200

# 	* print 'pending list = ' , fun(response)

# Scenario: list by status 'AVAILABLE'
# 	* param status = 'available'
	
# 	Given url SITE
# 	And path ENDPOINT_PET, '/findByStatus'
#   When method GET
#   Then status 200

# 	* print 'available list = ' , fun(response)

#!SECTION

################################################
# SECTION SAME ACTIONS WITH A OUTLINE SCENARIO #
################################################

Scenario Outline: param: <status>

	Given url SITE
	And path ENDPOINT_PET, '/findByStatus'
	And params { status: #(status) }
	When method get
	Then status 200

 	* print '<status> list = ' , 	fun(response)

	Examples:
		| status    |
		| sold      |
		| pending   |
		| available |

#!SECTION