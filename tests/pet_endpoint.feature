
@pet_endpoint
Feature: PET ENDPOINT CRUD TESTS

Background:	

	# Reading petdata for the next requests (POST doesn't need it)
	* def data = read('../target/petdata.json')
Scenario Outline: <name>

	Given url SITE
	And path <uri>
	And request <payload>
	When method <verb>
	Then status <expected>
	
	Examples:
	|	name			|	verb		|	uri												|	expected	|	payload															|
	|	get-KO		|	get			|	'pet/12323879623498'			|	404				|	null																|
	|	post-KO		|	post		|	'pet/'										|	404				|	{err:"1"}														|
	|	put-KO		|	put			|	'pet/12323879623498'			|	404				|	{"name": "verano","status":"sold"}	|
	|	delete-KO	|	delete	|	'pet/12323879623498'			|	404				|	null																|



Scenario: POST

	* print '## POST ##'

	Given url SITE
	And path ENDPOINT_PET
	And request {"name": "verano","photoUrls": ["string",],"status":"available"}
	When method post
	Then status 200
	And response.name == "verano"
	
	* def petId = response.id

	* print 'new pet => ', response
	
	* if (responseStatus === (200 || 201)) karate.write(karate.pretty({"petId": response.id}), './petdata.json')

	# NOTE - Is possible to include this scenario in the previous outline one, but for simplicity with the OK-tests, it is out of it.
	#
	#	|	post-OK		|	post		|	'pet/'										|	200				|	{"name": "verano","photoUrls": ["string",],"status":"available"}	|
	#
	
	
Scenario: GET
	
	* print '## GET ##'
	
	Given url SITE
	And path ENDPOINT_PET, data.petId
	When method get
	Then status 200
	And match data.petId == response.id
	And match response.name == "verano"
	And match response.status == "available"


Scenario: GET AS A LIST (find by status available as it was created)

	* print '## GET AS A LIST ##'

	Given url SITE
	And path ENDPOINT_PET, '/findByStatus'
	And param status = 'available'
	When method get
	Then status 200
	And match karate.toString(response) contains karate.toString(data.petId)


Scenario: PUT

	* print '## PUT ##'

	Given url SITE
	And path ENDPOINT_PET, data.petId
	And request {"name": "v3r4n0", "status": "sold"}
	When method put
	Then status 200
	And match data.petId == response.id
	And match response.name == "v3r4n0"
	And match response.status == "sold"

	# Double check with a GET request
	Given url SITE
	And path ENDPOINT_PET, data.petId
	When method get
	Then status 200
	And match data.petId == response.id
	And match response.name == "v3r4n0"
	And match response.status == "sold"


Scenario: DELETE

	* print '## DELETE ##'

	Given url SITE
	And path ENDPOINT_PET, data.petId
	When method delete
	Then status 200

	# Double check with a GET request
	Given url SITE
	And path ENDPOINT_PET, data.petId
	When method get
	Then status 404
