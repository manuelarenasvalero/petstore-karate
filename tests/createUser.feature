@createUser

Feature: Creation of a new user

Background:
  * def rand_number = function(max){ return Math.floor(Math.random() * max) }
  * def data = read('../data.json')


Scenario: create new user with random username and check

  Given url SITE
  And path ENDPOINT_USER

  # SECTION - Building payload

  * def username = rand_number(999999) + ''
  * print 'created username: ', username
  * def payload =
    """
    {
      "username": '#(username)',
      "firstName": '#(data.firstName)',
      "lastName": '#(data.lastName)',
      "email": '#(data.email)',
      "password": '#(data.password)',
      "phone": '#(data.phone)',
      "userStatus": 0
    }
    """

  # !SECTION - Building payload

  And request payload
  When method POST
  Then status 200
  # REVIEW - Return status should be 201 on a create element request

  * print 'created id: ' , response.message

  #################
  # RETRIEVE DATA #
  #################

  Given url SITE
  And path ENDPOINT_USER, username
  When method GET
  Then status 200
  And match response contains payload

  * print response
