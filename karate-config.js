function  fn () {

  var config = {
     
     SITE: 'https://petstore.swagger.io/v2/',
     ENDPOINT_USER: 'user',
     ENDPOINT_PET: 'pet',
   }
  
  karate.configure( 'connectTimeout' , 5000 );
  karate.configure( 'readTimeout' , 5000 );
  karate.configure( 'headers' , { Accept: 'application/json'});
  karate.configure( 'retry' , { count: 5, interval: 5000 });
  karate.configure('logPrettyRequest', true);
  karate.configure('logPrettyResponse', true);

   return  config ;
}