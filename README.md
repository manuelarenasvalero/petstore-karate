# swagger-api-petstore

## Description
This is my test project for an automation over an API using Karate.

## Installation
Select a folder on your host.

Open a terminal and text:

`git clone https://gitlab.com/tests-with-karate/swagger-api-petstore.git`

## Usage
Check you have the Karate Standalone file in your main path of the project.

Make sure you have configured the system environment variable for java.

Launch the tests using the console:

`java -jar karate.jar tests/`

or

`java -jar karate.jar tests/myNewTest.feature`

Open the results with your browser:

`target/karate-reports/karate-summary.html`

## Authors and acknowledgment
https://petstore.swagger.io

https://github.com/karatelabs/karate
